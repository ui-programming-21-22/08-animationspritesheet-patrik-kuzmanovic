
(function() { "use strict";

  const SPRITE_SIZE = 16;


  function playSound()
{
  let goat = new Audio("bounce.mp3");
}

  
  class Animation {
        constructor(frame_set, delay) {

            this.count = 0; 
            this.delay = delay; 
            this.frame = 0; 
            this.frame_index = 0; 
            this.frame_set = frame_set; 

        }
       
        change(frame_set, delay = 15) {

            if (this.frame_set != frame_set) { 

                this.count = 0; 
                this.delay = delay; 
                this.frame_index = 0; 
                this.frame_set = frame_set; 
                this.frame = this.frame_set[this.frame_index]; 

            }

        }
        
        update() {

            this.count++; 

            if (this.count >= this.delay) { 

                this.count = 0; 

                this.frame_index = (this.frame_index == this.frame_set.length - 1) ? 0 : this.frame_index + 1;
                this.frame = this.frame_set[this.frame_index]; 
                currentTime(loop);
                playSound();

            }

        }
    }


  var buffer, controller, display, loop, player, render, resize, sprite_sheet;

  buffer = document.createElement("canvas").getContext("2d");
  display = document.querySelector("canvas").getContext("2d");

  controller = {

    left:  { active:false, state:false },
    right: { active:false, state:false },
    up:    { active:false, state:false },
    space: { active:false, state:false }, 

    keyUpDown:function(event) {

      var key_state = (event.type == "keydown") ? true : false;

      switch(event.keyCode) {

        case 37:// left key

          if (controller.left.state != key_state) controller.left.active = key_state;
          controller.left.state  = key_state;

        break;
        case 38:// up key

          if (controller.up.state != key_state) controller.up.active = key_state;
          controller.up.state  = key_state;

        break;
        case 39:// right key

          if (controller.right.state != key_state) controller.right.active = key_state;
          controller.right.state  = key_state;

        break;
        case 40:

          if (controller.down.state != key_state) {
            console.log("Rocket");
            playerImage.src = "spaceship.png";
        }
    

      }

    }

  };

  
  player = {

    animation:new Animation(),
    jumping:true,
    height:16,    width:16,
    x:0,          y:40 - 18,
    x_velocity:0, y_velocity:0

  };

  sprite_sheet = {

    frame_sets:[[0, 1], [2, 3], [4, 5]],
    image:new Image()

  };

  loop = function(time_stamp) {

    if (controller.up.active && !player.jumping) {

      controller.up.active = false;
      player.jumping = true;
      player.y_velocity -= 2.5;

    }

    if (controller.left.active) {

      
      player.animation.change(sprite_sheet.frame_sets[2], 15);
      player.x_velocity -= 0.05;

    }

    if (controller.right.active) {

      player.animation.change(sprite_sheet.frame_sets[1], 15);
      player.x_velocity += 0.05;

    }

    
    if (!controller.left.active && !controller.right.active) {

      player.animation.change(sprite_sheet.frame_sets[0], 20);

    }

    if (controller.space.active) {

      goat.play();
    }

    player.y_velocity += 0.25;

    player.x += player.x_velocity;
    player.y += player.y_velocity;
    player.x_velocity *= 0.9;
    player.y_velocity *= 0.9;

    if (player.y + player.height > buffer.canvas.height - 2) {

      player.jumping = false;
      player.y = buffer.canvas.height - 2 - player.height;
      player.y_velocity = 0;

    }

    if (player.x + player.width < 0) {

      player.x = buffer.canvas.width;

    } else if (player.x > buffer.canvas.width) {

      player.x = - player.width;

    }

    player.animation.update();

    render();

    window.requestAnimationFrame(loop);

  };

  render = function() {

    buffer.fillStyle = "#7ec0ff";
    buffer.fillRect(0, 0, buffer.canvas.width, buffer.canvas.height);
    buffer.strokeStyle = "#8ed0ff";
    buffer.lineWidth = 10;
    buffer.beginPath();
    buffer.moveTo(0, 0);
    buffer.bezierCurveTo(40, 20, 40, 0, 80, 0);
    buffer.moveTo(0, 0);
    buffer.bezierCurveTo(40, 20, 40, 20, 80, 0);
    buffer.stroke();
    buffer.fillStyle = "#009900";
    buffer.fillRect(0, 36, buffer.canvas.width, 4);

    buffer.drawImage(sprite_sheet.image, player.animation.frame * SPRITE_SIZE, 0, SPRITE_SIZE, SPRITE_SIZE, Math.floor(player.x), Math.floor(player.y), SPRITE_SIZE, SPRITE_SIZE);

    display.drawImage(buffer.canvas, 0, 0, buffer.canvas.width, buffer.canvas.height, 0, 0, display.canvas.width, display.canvas.height);

  };

  resize = function() {

    display.canvas.width = document.documentElement.clientWidth - 32;

    if (display.canvas.width > document.documentElement.clientHeight) {

      display.canvas.width = document.documentElement.clientHeight;

    }

    display.canvas.height = display.canvas.width * 0.5;

    display.imageSmoothingEnabled = false;

  };

  function currentTime()
    {
        const months = ["January", "February", 
        "March", "April", "May", "June", "July", "August", 
        "September", "October", "November", "December"]; 

        let date = new Date();
        let hour = date.getHours();
        let min = date.getMinutes();
        let sec = date.getSeconds();
        let day = date.getDate();
        let month = months[date.getMonth()];
        let currentyear = date.getFullYear();
        let dayOrder = "th";
        let secNo = "";
        let minNo = "";
        let hourNo = "";

        switch(day)
        {
            case 1:
                dayOrder = "st";
                break;
            case 2:
                dayOrder = "nd";
                break;
            case 3:
                dayOrder = "rd";
                break;
            case 21:
                dayOrder = "st";
                break;
            case 22: 
                dayOrder = "nd";
                break;
            case 23: 
                dayOrder = "rd";
                break;
            case 31:
                dayOrder = "st";
                break;
            default:
                dayOrder = "th";
        }

        if (sec < 10)
        {
            secNo = "0";
        }
        if (min < 10)
        {
            minNo = "0";
        }
        if (hour < 10)
        {
            hourNo = "0";
        }
        
      document.getElementById("time").innerHTML = hourNo + hour + " : " + minNo + min + " : " + secNo + sec ;
      document.getElementById("date").innerHTML = day + dayOrder + " " + month + " " + currentyear;

    }

  buffer.canvas.width = 80;
  buffer.canvas.height = 40;

  window.addEventListener("resize", resize);

  window.addEventListener("keydown", controller.keyUpDown);
  window.addEventListener("keyup", controller.keyUpDown);

  resize();

  sprite_sheet.image.addEventListener("load", function(event) {

    window.requestAnimationFrame(loop);

  });

  sprite_sheet.image.src = "animation.png";

})();